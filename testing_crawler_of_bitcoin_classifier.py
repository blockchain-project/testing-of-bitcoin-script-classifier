import datetime
import json
import os
import queue

from bloxplorer import bitcoin_explorer as explorer

from copied_bitcoin_classifier.bitcoin import Transaction


def crawl(start_transaction: [str], n=10000, timeout=0.01, prevout=False):
    counter = 0

    # Set up queue of transactions to crawl
    transaction_queue = queue.Queue()
    for s in start_transaction:
        transaction_queue.put(s)

    # All variables to record in the crawl
    input_scripts = dict()
    output_scripts = dict()
    input_scripts_to_txid = dict()
    output_scripts_to_txid = dict()
    i_classification_dic = {'unknown':[]}
    o_classification_dic = {'unknown':[]}
    prev_outputs_to_input_scripts = {}
    i_class_to_occ_dic = dict()
    o_class_to_occ_dic = dict()

    # Actual crawl
    # try:
    while not transaction_queue.empty():
        txid = transaction_queue.get()
        # in coin base transaction, we could get such a txid which is not an actual transaction
        if txid == '0000000000000000000000000000000000000000000000000000000000000000':
            continue
        # Fetch the transaction
        try:
            explorer_data = explorer.tx.get(txid, timeout=10)
        except Exception as e:
            print(e)
            continue
        # Parse it.
        # ## !!!! This steps calls the class from the bitcoin classification endpoint. In this operation all
        # scripts are classified.
        transaction = Transaction(explorer_data)

        # Record the input scripts
        for i in transaction.input_scripts:
            opcode_string = i.opcode_string()
            if opcode_string not in input_scripts.keys():
                # count and add script to input scripts
                input_scripts[opcode_string] = 1
                # add the transaction ID to this script, to get examples of transactions
                input_scripts_to_txid[opcode_string] = {transaction.txid}

                # Get previous script of input transaction
                prev = None
                try:
                    prev_outputs_to_input_scripts[opcode_string] = i.prevout.opcode_string()
                    prev = i.prevout.opcode_string()
                except Exception as e:
                    pass
                # print if classification is unknown, this can be commented for later.
                if 'unknown' in i.classifier.classification:
                    print("Unknown classification of input script: " +
                          "{} with script {} and previous script {}".format(transaction.txid, opcode_string, prev))
                    current_unknown_scripts = i_classification_dic['unknown']
                    current_unknown_scripts.append(opcode_string)
                    i_classification_dic['unknown'] = current_unknown_scripts
                classific = i.classifier.classification
                if hasattr(i, 'inner_redeemscript'):
                    classific+= " redeem: {}".format(i.inner_redeemscript.classifier.classification)
                if hasattr(i, 'inner_witnessscript_asm'):
                    classific += " witness: {}".format(i.inner_witnessscript_asm.classifier.classification)
                i_classification_dic[opcode_string] = {classific}

                occ = i_class_to_occ_dic.get(classific, 0)
                occ += 1
                i_class_to_occ_dic[classific] = occ
            else:
                input_scripts[opcode_string] += 1
                prev = input_scripts_to_txid[opcode_string]
                prev.add(transaction.txid)
                input_scripts_to_txid[opcode_string] = prev

                classific = i.classifier.classification
                if hasattr(i, 'inner_redeemscript'):
                    classific += " redeem: {}".format(i.inner_redeemscript.classifier.classification)
                if hasattr(i, 'inner_witnessscript_asm'):
                    classific += " witness: {}".format(i.inner_witnessscript_asm.classifier.classification)
                current =  i_classification_dic[opcode_string]
                current.add(classific)
                i_classification_dic[opcode_string] = current

                occ = i_class_to_occ_dic.get(classific, 0)
                occ += 1
                i_class_to_occ_dic[classific] = occ

        for o in transaction.output_scripts:
            opcode_string = o.opcode_string()
            if opcode_string not in output_scripts.keys():
                output_scripts[opcode_string] = 1
                output_scripts_to_txid[opcode_string] = {transaction.txid}
                # check if we can classify it already
                if 'unknown' in o.classifier.classification:
                    print("Unknown classification of ouput script: " +
                          "{} with script {} ".format(transaction.txid, opcode_string))
                    current_unknown_scripts = o_classification_dic['unknown']
                    current_unknown_scripts.append(opcode_string)
                    o_classification_dic['unknown'] = current_unknown_scripts
                if 'null_data' in o.classifier.classification:
                    try:
                        print("Null output: {} classification {} protocol {}".format(transaction.txid, o.classifier.classification, o.classifier.data['protocol']))
                    except Exception as e:
                        pass
                o_classification_dic[opcode_string] = {o.classifier.classification}

                occ = o_class_to_occ_dic.get(o.classifier.classification, 0)
                occ += 1
                o_class_to_occ_dic[o.classifier.classification] = occ
            else:
                output_scripts[opcode_string] += 1
                prev = output_scripts_to_txid[opcode_string]
                prev.add(transaction.txid)
                output_scripts_to_txid[opcode_string] = prev

                curreent = o_classification_dic[opcode_string]
                curreent.add(o.classifier.classification)
                o_classification_dic[opcode_string] = curreent

                occ = o_class_to_occ_dic.get(o.classifier.classification, 0)
                occ += 1
                o_class_to_occ_dic[o.classifier.classification] = occ

    return input_scripts, output_scripts, input_scripts_to_txid, output_scripts_to_txid, i_classification_dic, \
           o_classification_dic, prev_outputs_to_input_scripts, i_class_to_occ_dic, o_class_to_occ_dic



def get_transactions_from_file():
    with open('transactions_of_100_blocks.txt') as f:
        return f.read().splitlines()

def get_transactions_of_blocks(height=676152, number=10):
    # get all transactions from 2021-03-24; so from block 676001 til block 676151
    transactions = []
    blocks = []
    print("Get blocks")
    for i in range(0, number):
        try:
            explorer_data = explorer.blocks.get_blocks(height, timeout=10)
            these_blocks = [b['id'] for b in explorer_data.data]
            blocks.extend(these_blocks)
            height = explorer_data.data[-1]['height']
        except Exception as e:
            print(e)
            print("transactions = ", transactions)
    print("Get transactions")
    for block in blocks:
        try:
            explorer_data = explorer.blocks.get_txids(block)
            transactions.extend(explorer_data.data)
        except Exception as e:
            print(e)
            print("transactions = ", transactions)
    with open('ttransactions_of_'+ str(len(blocks)) + '_blocks.txt', 'w') as fpset:
        for t in transactions:
            fpset.write('%s\n' % t)
    return transactions


def save_dictionaries_to_file(mydir, i_scripts, o_scripts,  i_scripts_txid, o_scripts_txid, i_classification,
                              o_classification, prev_input_scripts, i_class_to_occ_dic, o_class_to_occ_dic ):
    with open(os.path.join(mydir, 'input_scripts.json'), 'w') as input_file:
        json.dump(i_scripts, input_file)
    with open(os.path.join(mydir, 'output_scripts.json'), 'w') as output_file:
        json.dump(o_scripts, output_file)
    with open(os.path.join(mydir, 'input_classification_occurrences.json'), 'w') as i_class_o_file:
        json.dump(i_class_to_occ_dic, i_class_o_file)
    with open(os.path.join(mydir, 'output_classification_occurences.json'), 'w') as o_class_o_file:
        json.dump(o_class_to_occ_dic, o_class_o_file)

    print("\nPRINTING INPUT SCRIPTS")
    for k, v in i_scripts.items():
        print("Script: {},  Occurences: {}".format(k, v))
    print("\nPRINTING OUTPUT SCRIPTS")
    for k, v in o_scripts.items():
        print("Script: {},  Occurences: {}".format(k, v))

    two_i_script_txid = {}
    print("\nPRINTING INPUT SCRIPTS TO TXIDs")
    for k, v in i_scripts_txid.items():
        v = list(v)
        print("Script: {} \n Transaction with it: {}".format(k, v[0]))
        i_scripts_txid[k] = v
        if len(v) > 1:
            two_i_script_txid[k] = v[0:2]
        else:
            two_i_script_txid[k] = v[0]
    two_o_script_txid = {}
    print("\nPRINTING OUTPUT SCRIPTS TO TXIDs")
    for k, v in o_scripts_txid.items():
        v = list(v)
        print("Script: {}\n Transaction with it: {}".format(k, v[0]))
        o_scripts_txid[k] = v
        if len(v) > 1:
            two_o_script_txid[k] = v[0:2]
        else:
            two_o_script_txid[k] = v[0]

    print("\nPRINTING CLASSIFICATION INPUT SCRIPTS")
    for k, v in i_classification.items():
        v = list(v)
        print("Script: {},  Classification: {}".format(k, v))
        i_classification[k] = v
    print("\nPRINTING CLASSIFICATION OUTPUT SCRIPTS")
    for k, v in o_classification.items():
        v = list(v)
        print("Script: {},  Classification: {}".format(k, v))
        o_classification[k] = v

    with open(os.path.join(mydir, 'input_scripts_to_txid.json'), 'w') as input_tx_file:
        json.dump(i_scripts_txid, input_tx_file)
    with open(os.path.join(mydir, 'output_scripts_to_txid.json'), 'w') as output_tx_file:
        json.dump(o_scripts_txid, output_tx_file)
    with open(os.path.join(mydir, 'input_scripts_to_txid_readable.json'), 'w') as input_tx_file_r:
        json.dump(two_i_script_txid, input_tx_file_r)
    with open(os.path.join(mydir, 'output_scripts_to_txid_readable.json'), 'w') as output_tx_file_r:
        json.dump(two_o_script_txid, output_tx_file_r)
    with open(os.path.join(mydir, 'output_script_classification.json'), 'w') as output_class_file:
        json.dump(o_classification, output_class_file)
    with open(os.path.join(mydir, 'input_script_classification.json'), 'w') as input_class_file:
        json.dump(i_classification, input_class_file)

    print("\nPRINTING PREVIOUS OUTPUT SCRIPTS TO ALL INPUT SCRIPTS IF NEWLY CRAWLED")
    for k, v in prev_input_scripts.items():
        print("Script: {},  Previous scripts: {}".format(k, v))

def read_json_files(path):
    with open(path) as f:
        return json.load(f)

def get_dics_from_file(mydir):
    i_scripts = read_json_files(os.path.join(mydir, 'input_scripts.json'))
    o_scripts = read_json_files(os.path.join(mydir, 'output_scripts.json'))
    o_classification = read_json_files(os.path.join(mydir, 'output_script_classification.json'))
    i_classification = read_json_files(os.path.join(mydir, 'input_script_classification.json'))
    i_class_to_occ_dic =  read_json_files(os.path.join(mydir, 'input_classification_occurrences.json'))
    o_class_to_occ_dic =  read_json_files(os.path.join(mydir, 'output_classification_occurences.json'))
    i_scripts_txid = read_json_files(os.path.join(mydir, 'input_scripts_to_txid.json'))
    o_scripts_txid = read_json_files(os.path.join(mydir, 'output_scripts_to_txid.json'))
    prev_input_scripts = {} # not yet saved
    return i_scripts, o_scripts, i_classification, o_classification, i_class_to_occ_dic, o_class_to_occ_dic,  i_scripts_txid, o_scripts_txid,  prev_input_scripts,

def get_transactions_from_file(number=80):
    filepath = "transactions_of_"+ str(number) + "_blocks.txt"
    with open(filepath) as f:
        return f.read().splitlines()


if __name__ == "__main__":
    ### Get FRESH classifications: say False, False at GET_DICS/TXID_FROM_FILEand specify a starting height
    # All results are printed to the console and saved in json files in the newly created folder with todays date.
    # When newly crawling all unknown classifications, errors and 'unspendable output' are printed with the
    # respective ID. If an input script is classified as unknnown, the previous output script is printed. If the latter
    # None, then the transaction was part of a coinbase transaction.

    GET_DICS_FROM_FILE = False
    GET_TXID_FROM_FILE = True
    dir_of_stored_dics = "2021-04-14_19-41-54_of_block_ 676152" # results when crawling 10 blocks starting at 676152
    file_number_transaction = 10   # to use transaction ids from file transactions_of_<10>_blocks.txt
    number_of_10_blocks_to_get = 1 # how many blocks to get; will be multiplied by 10 due to library specs
    starting_height = 676152    # From this height til numberx10 more recent blocks are collected

    ### DO NOT change
    mydir = os.path.join(os.getcwd(), datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    os.makedirs(mydir)

    if GET_DICS_FROM_FILE:
        i_scripts, o_scripts, i_classification, o_classification, i_class_to_occ_dic, o_class_to_occ_dic, i_scripts_txid, o_scripts_txid,  prev_input_scripts, = get_dics_from_file(dir_of_stored_dics)
    else:
        if GET_TXID_FROM_FILE:
            ## Get txids from file
            txids = get_transactions_from_file(file_number_transaction)
        else:
            ## Get txids from blockchain
            txids = get_transactions_of_blocks(height=starting_height, number=1)
        print("Crawl transactions: ", len(txids))
        i_scripts, o_scripts, i_scripts_txid, o_scripts_txid, i_classification, o_classification, prev_input_scripts, i_class_to_occ_dic, o_class_to_occ_dic = crawl(
            txids, prevout=False)

    print("Save dictionaries to JSON")
    # save  and print dictionaries
    save_dictionaries_to_file(mydir, i_scripts, o_scripts, i_scripts_txid, o_scripts_txid, i_classification, o_classification, prev_input_scripts, i_class_to_occ_dic, o_class_to_occ_dic)

    print("\nFinished. All values should have been printed to the console.")

