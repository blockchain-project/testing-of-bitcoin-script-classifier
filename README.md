# Testing of bitcoin script classifier

This project tests our Bitcoin Script Classifier by crawling transactions of the Bitcoin blockchain.

## Testing
The testing crawler contains all logic to crawl transactions from the blockchain starting at a block with a specified height. Then, the transactions are parsed and classified using the Bitcoin Script Classifier. For testing purposes these classes have been copied from the original purpose. 
The only change to the Bitocin Script Classification is the addition of this function in ```class Script``` of ```bitcoin.py```:

```python
    def opcode_string(self):
        return " ".join([o.code for o in self.opcodes])
```
## Crawling
In the main function of the crawler, the crawl is defined out of 3 possibilities: either new transactions are crawled from a specified block height, the transaction IDs are taken from a .txt file and then classified, of the classification has already been done and is taken from stored json file and printed again to the console. 

## Results
```2021-04-14_19-41-54_of_block_ 676152``` contains the result json from the latest test. All transactions of 10 blocks have been crawled starting from block 676152, all involved scripts are classified. There are different result json files. For all output scripts and all input scripts the following json files are present:  

- Opcodes of script and how they are classified
- Opcodes of script and transaction IDs that contain them; one file containing all transaction IDs that had these opcodes and another file containing only a single txid for every opcode
- List of all Opcodes present
- occurrences of classification results