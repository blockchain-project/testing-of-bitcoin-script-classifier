## #################################################
## THIS CLASS HAS BEEN COPIED FROM THE blockchain-project/bitcoin-script-classifier PROJECT
## for testing purposes. For the latest version check out the original project.
## This version of this class is from 30/03/2021
####################################################


from bloxplorer.transactions import Transactions
from json import JSONEncoder
from opcodes import *
from copied_bitcoin_classifier.opcodes import *
import queue


def get_opcodes_from_string(opcode_str: str):
    if opcode_str == "":
        return []
    res = []

    splitted = opcode_str.split()
    op = splitted[0]
    data = []

    for o in splitted[1:]:
        if o[:2] == "OP":
            res.append(get_opcode_by_name(op)(op, data))
            op = o
            data = []
        else:
            data.append(o)

    res.append(get_opcode_by_name(op)(op, data))
    return res

class Script:
    opcodes = None
    opcode_str = None
    classifier = None

    def __init__(self, script: dict):
        isInputScript = 'txid' in script
        script_str = script["scriptsig_asm"] if isInputScript else script["scriptpubkey_asm"]

        if isInputScript:
            if "prevout" in script and script["prevout"]:
                self.prevout = Script(script["prevout"])
            if "inner_redeemscript_asm" in script and script["inner_redeemscript_asm"]:
                self.inner_redeemscript = Script({"scriptpubkey_asm": script["inner_redeemscript_asm"]})
            if "inner_witnessscript_asm" in script and script["inner_witnessscript_asm"]:
                self.inner_witnessscript_asm = Script({"scriptpubkey_asm": script["inner_witnessscript_asm"]})

        from copied_bitcoin_classifier.classifier import Classifier

        self.opcode_str = script_str
        self.opcodes = get_opcodes_from_string(script_str)
        self.classifier = Classifier(self.opcodes)

        if isInputScript and self.classifier.classification == 'unknown':
            if "is_coinbase" in script and script["is_coinbase"]:
                self.classifier.data = { "type": "coinbase"}
            elif "prevout" in script and script["prevout"]:
                self.classifier = self.prevout.classifier

    def __str__(self):
        return "Script: {}".format(self.opcode_str)

    def opcode_string(self):
        return " ".join([o.code for o in self.opcodes])


class Transaction:
    txid = 0
    input_scripts = None
    output_scripts = None

    def __init__(self, trans: Transactions):
        data = trans.data
        self.txid = data["txid"]
        self.input_scripts = [Script(inputs) for inputs in data["vin"]]
        self.output_scripts = [Script(outputs) for outputs in data["vout"]]

    def __str__(self):
        return "Transaction with id: {}".format(self.txid)


class TransactionEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


class ScriptStep:
    id = 0
    remaining_script = ""
    current_opcode = ""
    stack = []
    error = ""

    def __init__(self, id: int, remaining_script: str, current_opcode: OpCode, stack: [str], error: str = None):
        if error != None:
            self.error = error
            self.current_opcode = current_opcode
            return
        self.id = id
        self.remaining_script = remaining_script
        self.current_opcode = current_opcode
        self.stack = stack
        self.error = error


class ScriptStepEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


class ScriptInterpreter:
    stack = None
    opcodes = None

    def __init__(self, opcodes: [OpCode]):
        self.stack = queue.LifoQueue()
        self.opcodes = opcodes

    def calculate_remaining_script(self, opcodes: [OpCode]):
        return " ".join([str(o) for o in opcodes])

    def execute(self):
        counter = 0
        res = []
        opc = self.opcodes

        res.append(ScriptStep(
            counter,
            self.calculate_remaining_script(opc),
            None,
            list(self.stack.queue)
        ))

        for o in opc:
            counter += 1
            try:
                o.execute(self.stack)
                res.append(ScriptStep(
                    counter,
                    self.calculate_remaining_script(opc[counter:]),
                    o,
                    list(self.stack.queue)
                ))
            except NotImplementedError as nie:
                res.append(ScriptStep(
                    counter,
                    self.calculate_remaining_script(opc[counter:]),
                    o,
                    list(self.stack.queue),
                    "This opcode is not implemented"
                ))
                break
            except DisabledOpCodeException as dce:
                res.append(ScriptStep(
                    counter,
                    self.calculate_remaining_script(opc[counter:]),
                    o,
                    list(self.stack.queue),
                    "This opcode is disabled"
                ))
                break
            except Exception as e:
                res.append(ScriptStep(
                    counter,
                    self.calculate_remaining_script(opc[counter:]),
                    o,
                    list(self.stack.queue),
                    "Something went wrong!"
                ))
                break

        return res


